const express = require("express");
const cors = require('cors');

const app = express();
const port = 3000;
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);
app.use(cors({
    origin: '*'
}));
const users = [
  {
    name: "admin",
    email: "admin@gmail.com",
    password: "1234",
  },
];

app.get("/", (req, res) => {
  res.send("Hello World!");
});
app.post("/login", (req, res) => {
  let email = req.body.email;
  let password = req.body.password;
  let user = "";
  if (email) {
    user = findUser(email);
  }
  if (user?.password === password) {
    return res.json({ data: user, message: "user found" });
  }
  return res.json({ data: undefined, message: "wrong password or username" });
});
app.post("/register", (req, res) => {
  let email = req.body.email;
  let password = req.body.password;
  let name = req.body.name;
  let user = "";
  if (email) {
    user = findUser(email);
  }
  if (user) {
    return res.json({ data: undefined, message: "user already exists" });
  }
  users.push({ email, password, name });
  return res.json({ data: findUser(email), message: "user saved" });
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});

function findUser(email) {
  return users.filter((user) => user.email === email)[0];
}
